Role Name
ansible-role-docker
=========



Requirements
------------

ansible:2.16.3
    default:24.2.0 from molecule
    docker:2.1.0 from molecule_docker requiring collections: community.docker>=3.0.2 ansible.posix>=1.4.0


Role Variables
--------------

# defaults file for ansible-role-docker
variable_execute_loading: "false"
Flag_success_script: "false"
docker_required_packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - gnupg
  - lsb-release
docker_list_packages:  
  - docker-ce
  - docker-ce-cli
  - containerd.io
apache_site_name: ansible
# vars file for ansible-role-docker
templates_role: "{{ role_path }}/templates/docker"

Dependencies
------------

роль работает на пк с доступом к интернету

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

ansible-playbook roling.yaml -i ./inventories/dev/hosts



License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
